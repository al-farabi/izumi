﻿using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Context
{
    public class IzumiContext : DbContext
    {
        public IzumiContext(DbContextOptions<IzumiContext> options)
            : base(options)
        {
        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Waiter> Waiters { get; set; }
        public DbSet<WaiterReviews> WaiterReviews { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WaiterReviews>()
                .HasKey(t => new { t.ReviewId, t.WaiterId });

            modelBuilder.Entity<WaiterReviews>()
                .HasOne(pt => pt.Review)
                .WithMany(p => p.WaiterReviews)
                .HasForeignKey(pt => pt.ReviewId);

            modelBuilder.Entity<WaiterReviews>()
                .HasOne(pt => pt.Waiter)
                .WithMany(t => t.WaiterReviews)
                .HasForeignKey(pt => pt.WaiterId);
        }
    }
}