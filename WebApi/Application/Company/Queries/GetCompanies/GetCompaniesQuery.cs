﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;
using WebApi.Infrastructure.Extensions;

namespace WebApi.Application.Company.Queries.GetCompanies
{
    public class GetCompaniesQuery : IRequest<List<CompanyResponse>>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }

        public class Handler : IRequestHandler<GetCompaniesQuery, List<CompanyResponse>>
        {
            private readonly IzumiContext _context;
            private readonly IMapper _mapper;

            public Handler(IzumiContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<CompanyResponse>> Handle(GetCompaniesQuery request, CancellationToken cancellationToken)
            {
                var companies = await _context.Companies
                    .AsNoTracking()
                    .ExcludeDeleted()
                    .PageBy(c => c.Created, request.Page, request.PageSize)
                    .ProjectTo<CompanyResponse>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                return companies;
            }
        }
    }
}