﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;
using WebApi.Infrastructure.Exceptions;
using WebApi.Infrastructure.Extensions;

namespace WebApi.Application.Company.Queries.GetCompany
{
    public class GetCompanyQuery : IRequest<CompanyResponse>
    {
        public Guid Id { get; set; }

        public class Handler : IRequestHandler<GetCompanyQuery, CompanyResponse>
        {
            private readonly IzumiContext _context;
            private readonly IMapper _mapper;

            public Handler(IzumiContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<CompanyResponse> Handle(GetCompanyQuery request, CancellationToken cancellationToken)
            {
                var company = await _context.Companies
                    .AsNoTracking()
                    .ExcludeDeleted()
                    .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken: cancellationToken);

                if (company == null)
                {
                    throw new NotFoundException(nameof(company), request.Id);
                }

                return _mapper.Map<CompanyResponse>(company);
            }
        }

        public class Validator : AbstractValidator<GetCompanyQuery>
        {
            public Validator()
            {
                RuleFor(c => c.Id).NotEmpty();
            }
        }
    }
}