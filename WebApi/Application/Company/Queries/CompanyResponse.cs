﻿using System;
using AutoMapper;
using WebApi.Infrastructure.Automapper;

namespace WebApi.Application.Company.Queries
{
    public class CompanyResponse : IHaveCustomMapping
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Models.Company, CompanyResponse>();
        }
    }
}