﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;
using WebApi.Infrastructure;
using WebApi.Infrastructure.Exceptions;
using WebApi.Infrastructure.Extensions;
using WebApi.Models;

namespace WebApi.Application.Company.Commands
{
    public class UpdateCompanyCommand : IRequest
    {
        public Guid CompanyId { get; set; }
        public string Name { get; set; }
        public CompanyType CompanyType { get; set; }

        public class Handler : IRequestHandler<UpdateCompanyCommand>
        {
            private readonly IzumiContext _context;

            public Handler(IzumiContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UpdateCompanyCommand request, CancellationToken cancellationToken)
            {
                var now = DateTime.UtcNow;

                var company = await _context.Companies.ExcludeDeleted()
                    .FirstOrDefaultAsync(c => c.Id == request.CompanyId, cancellationToken);

                if (company == null)
                {
                    throw new NotFoundException(nameof(company), request.CompanyId);
                }

                company.CompanyType = request.CompanyType;
                company.Name = request.Name;

                company.Modified = now;
                company.ModifiedBy = User.Current;

                _context.Companies.Update(company);
                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }

        public class Validator : AbstractValidator<UpdateCompanyCommand>
        {
            public Validator()
            {
                RuleFor(c => c.Name).NotEmpty().Length(2, 50);
                RuleFor(c => c.CompanyId).NotEmpty();
                RuleFor(c => c.CompanyType).IsInEnum();
            }
        }
    }
}