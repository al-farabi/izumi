﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;
using WebApi.Infrastructure;
using WebApi.Infrastructure.Exceptions;
using WebApi.Infrastructure.Extensions;

namespace WebApi.Application.Company.Commands
{
    public class DeleteCompanyCommand : IRequest
    {
        public Guid CompanyId { get; set; }

        public class Handler : IRequestHandler<DeleteCompanyCommand>
        {
            private readonly IzumiContext _context;

            public Handler(IzumiContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteCompanyCommand request, CancellationToken cancellationToken)
            {
                var now = DateTime.UtcNow;
                var currentUser = User.Current;

                var company = await _context.Companies.ExcludeDeleted()
                    .FirstOrDefaultAsync(c => c.Id == request.CompanyId, cancellationToken);

                if (company == null)
                {
                    throw new NotFoundException(nameof(company), request.CompanyId);
                }

                company.IsDeleted = true;

                company.Modified = now;
                company.ModifiedBy = currentUser;

                _context.Companies.Update(company);
                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }

        public class Validator : AbstractValidator<DeleteCompanyCommand>
        {
            public Validator()
            {
                RuleFor(c => c.CompanyId).NotEmpty();
            }
        }
    }
}