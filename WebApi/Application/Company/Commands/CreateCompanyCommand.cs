﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using WebApi.Context;
using WebApi.Infrastructure;
using WebApi.Models;

namespace WebApi.Application.Company.Commands
{
    public class CreateCompanyCommand : IRequest
    {
        public string Name { get; set; }
        public CompanyType CompanyType { get; set; }

        public class Handler : IRequestHandler<CreateCompanyCommand>
        {
            private readonly IzumiContext _context;

            public Handler(IzumiContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(CreateCompanyCommand request, CancellationToken cancellationToken)
            {
                var now = DateTime.UtcNow;
                var currentUser = User.Current;

                var company = new Models.Company()
                {
                    CompanyType = request.CompanyType,
                    Name = request.Name,
                    Created = now,
                    IsDeleted = false,
                    CreatedBy = currentUser,
                    Modified = now,
                    ModifiedBy = currentUser,
                };

                await _context.Companies.AddAsync(company, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }

        public class Validator : AbstractValidator<CreateCompanyCommand>
        {
            public Validator()
            {
                RuleFor(c => c.Name).NotEmpty().Length(2, 50);
                RuleFor(c => c.CompanyType).IsInEnum();
            }
        }
    }
}