﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;
using WebApi.Infrastructure;
using WebApi.Infrastructure.Exceptions;
using WebApi.Infrastructure.Extensions;

namespace WebApi.Application.Waiter.Commands
{
    public class UpdateWaiterCommand : IRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid CompanyId { get; set; }
        public Guid WaiterId { get; set; }

        public class Handler : IRequestHandler<UpdateWaiterCommand>
        {
            private readonly IzumiContext _context;

            public Handler(IzumiContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UpdateWaiterCommand request, CancellationToken cancellationToken)
            {
                var now = DateTime.UtcNow;
                var currentUser = User.Current;

                var waiter = await _context.Waiters
                    .ExcludeDeleted()
                    .FirstOrDefaultAsync(c => c.Id == request.WaiterId && c.CompanyId == request.CompanyId,
                        cancellationToken);

                if (waiter == null)
                {
                    throw new NotFoundException(nameof(waiter), request.CompanyId);
                }

                waiter.FirstName = request.FirstName;
                waiter.LastName = request.LastName;

                waiter.Modified = now;
                waiter.ModifiedBy = currentUser;

                _context.Waiters.Update(waiter);
                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }

        public class Validator : AbstractValidator<UpdateWaiterCommand>
        {
            public Validator()
            {
                RuleFor(c => c.CompanyId).NotEmpty();
                RuleFor(c => c.WaiterId).NotEmpty();
                RuleFor(c => c.FirstName).NotEmpty().Length(2, 50);
                RuleFor(c => c.LastName).NotEmpty().Length(2, 50);
            }
        }

    }
}