﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;
using WebApi.Infrastructure;
using WebApi.Infrastructure.Exceptions;
using WebApi.Infrastructure.Extensions;

namespace WebApi.Application.Waiter.Commands
{
    public class DeleteWaiterCommand : IRequest
    {
        public Guid Id { get; set; }

        public class Handler : IRequestHandler<DeleteWaiterCommand>
        {
            private readonly IzumiContext _context;

            public Handler(IzumiContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteWaiterCommand request, CancellationToken cancellationToken)
            {
                var now = DateTime.UtcNow;
                var currentUser = User.Current;

                var waiter = await _context.Waiters
                    .ExcludeDeleted()
                    .FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

                if (waiter == null)
                {
                    throw new NotFoundException(nameof(waiter), request.Id);
                }

                waiter.IsDeleted = true;

                waiter.Modified = now;
                waiter.ModifiedBy = currentUser;

                _context.Waiters.Update(waiter);
                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }

        public class Validator : AbstractValidator<DeleteWaiterCommand>
        {
            public Validator()
            {
                RuleFor(c => c.Id).NotEmpty();
            }
        }
    }
}