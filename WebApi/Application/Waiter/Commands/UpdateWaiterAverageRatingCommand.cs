﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using WebApi.Application.Waiter.Queries.GetRatingByWaiterId;
using WebApi.Context;

namespace WebApi.Application.Waiter.Commands
{
    public class UpdateWaiterAverageRatingCommand : IRequest
    {
        public Models.Waiter Waiter { get; set; }

        public class Handler : IRequestHandler<UpdateWaiterAverageRatingCommand>
        {
            private readonly IzumiContext _context;
            private readonly IMediator _mediator;

            public Handler(IMediator mediator, IzumiContext context)
            {
                _mediator = mediator;
                _context = context;
            }

            public async Task<Unit> Handle(UpdateWaiterAverageRatingCommand request, CancellationToken cancellationToken)
            {
                var waiter = request.Waiter;
                var rating = await _mediator.Send(new GetRatingByWaiterIdQuery() { WaiterId = waiter.Id },
                    cancellationToken);

                waiter.AverageRating = rating;

                _context.Waiters.Update(waiter);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }

        public class Validator : AbstractValidator<UpdateWaiterAverageRatingCommand>
        {
            public Validator()
            {
                RuleFor(c => c.Waiter).NotNull();
            }
        }
    }
}