﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;
using WebApi.Infrastructure;
using WebApi.Infrastructure.Exceptions;
using WebApi.Infrastructure.Extensions;

namespace WebApi.Application.Waiter.Commands
{
    public class CreateWaiterCommand : IRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid CompanyId { get; set; }

        public class Handler : IRequestHandler<CreateWaiterCommand>
        {
            private readonly IzumiContext _context;

            public Handler(IzumiContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(CreateWaiterCommand request, CancellationToken cancellationToken)
            {
                var now = DateTime.UtcNow;
                var currentUser = User.Current;

                var company = await _context.Companies
                    .AsNoTracking()
                    .ExcludeDeleted()
                    .FirstOrDefaultAsync(c => c.Id == request.CompanyId, cancellationToken);

                if (company == null)
                {
                    throw new NotFoundException(nameof(company), request.CompanyId);
                }

                var waiter = new Models.Waiter()
                {
                    CompanyId = company.Id,
                    FirstName = request.FirstName,
                    LastName = request.LastName,

                    Created = now,
                    IsDeleted = false,
                    CreatedBy = currentUser,
                    Modified = now,
                    ModifiedBy = currentUser,
                };

                await _context.Waiters.AddAsync(waiter, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }

        public class Validator : AbstractValidator<CreateWaiterCommand>
        {
            public Validator()
            {
                RuleFor(c => c.CompanyId).NotEmpty();
                RuleFor(c => c.FirstName).NotEmpty().Length(2, 50);
                RuleFor(c => c.LastName).NotEmpty().Length(2, 50);
            }
        }
    }
}