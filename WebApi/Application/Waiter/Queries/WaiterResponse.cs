﻿using System;
using AutoMapper;
using WebApi.Infrastructure.Automapper;

namespace WebApi.Application.Waiter.Queries
{
    public class WaiterResponse : IHaveCustomMapping
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double Rating { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Models.Waiter, WaiterResponse>()
                .ForMember(c => c.Rating, options =>
                {
                    options.MapFrom(x => x.AverageRating);
                });
        }
    }
}