﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;
using WebApi.Infrastructure.Exceptions;
using WebApi.Infrastructure.Extensions;

namespace WebApi.Application.Waiter.Queries.GetWaiter
{
    public class GetWaiterQuery : IRequest<WaiterResponse>
    {
        public Guid CompanyId { get; set; }
        public Guid WaiterId { get; set; }

        public class Handler : IRequestHandler<GetWaiterQuery, WaiterResponse>
        {
            private readonly IzumiContext _context;
            private readonly IMapper _mapper;

            public Handler(IzumiContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<WaiterResponse> Handle(GetWaiterQuery request, CancellationToken cancellationToken)
            {
                var waiter = await _context.Waiters
                    .AsNoTracking()
                    .ExcludeDeleted()
                    .FirstOrDefaultAsync(
                        c => c.CompanyId == request.CompanyId && c.Id == request.WaiterId, cancellationToken);

                if (waiter == null)
                {
                    throw new NotFoundException(nameof(waiter), request.WaiterId);
                }

                return _mapper.Map<WaiterResponse>(waiter);
            }
        }

        public class Validator : AbstractValidator<GetWaiterQuery>
        {
            public Validator()
            {
                RuleFor(c => c.WaiterId).NotEmpty();
                RuleFor(c => c.CompanyId).NotEmpty();
            }
        }
    }
}