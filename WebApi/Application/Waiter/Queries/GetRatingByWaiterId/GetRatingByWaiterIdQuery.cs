﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;

namespace WebApi.Application.Waiter.Queries.GetRatingByWaiterId
{
    public class GetRatingByWaiterIdQuery : IRequest<double>
    {
        public Guid WaiterId { get; set; }

        public class Handler : IRequestHandler<GetRatingByWaiterIdQuery, double>
        {
            private readonly IzumiContext _context;

            public Handler(IzumiContext context)
            {
                _context = context;
            }

            public async Task<double> Handle(GetRatingByWaiterIdQuery request, CancellationToken cancellationToken)
            {
                var waiterReviews = _context.WaiterReviews
                    .AsNoTracking()
                    .Include(c => c.Review)
                    .Where(c => c.WaiterId == request.WaiterId);

                var average = waiterReviews.Average(c => c.Review.Rating);

                return await Task.FromResult(average);
            }
        }
    }
}