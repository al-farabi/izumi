﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;
using WebApi.Infrastructure.Extensions;

namespace WebApi.Application.Waiter.Queries.GetWaiters
{
    public class GetWaitersQuery : IRequest<List<WaiterResponse>>
    {
        public Guid CompanyId { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public class Handler : IRequestHandler<GetWaitersQuery, List<WaiterResponse>>
        {
            private readonly IzumiContext _context;
            private readonly IMapper _mapper;

            public Handler(IzumiContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<WaiterResponse>> Handle(GetWaitersQuery request, CancellationToken cancellationToken)
            {
                var waiters = await _context.Waiters
                    .AsNoTracking()
                    .ExcludeDeleted()
                    .Where(c => c.CompanyId == request.CompanyId)
                    .PageBy(c => c.Created, request.Page, request.PageSize)
                    .ProjectTo<WaiterResponse>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                return waiters;
            }
        }

        public class Validator : AbstractValidator<GetWaitersQuery>
        {
            public Validator()
            {
                RuleFor(c => c.CompanyId).NotEmpty();
            }
        }
    }
}