﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Application.Waiter.Commands;
using WebApi.Context;
using WebApi.Infrastructure;
using WebApi.Infrastructure.Exceptions;
using WebApi.Infrastructure.Extensions;
using WebApi.Models;

namespace WebApi.Application.Review.Commands
{
    public class CreateReviewCommand : IRequest
    {
        public Guid WaiterId { get; set; }
        public string Content { get; set; }
        public byte Rating { get; set; }

        public class Handler : IRequestHandler<CreateReviewCommand>
        {
            private readonly IzumiContext _context;
            private readonly IMediator _mediator;

            public Handler(IzumiContext context, IMediator mediator)
            {
                _context = context;
                _mediator = mediator;
            }

            public async Task<Unit> Handle(CreateReviewCommand request, CancellationToken cancellationToken)
            {
                var now = DateTime.UtcNow;
                var currentUser = User.Current;

                var waiter = await _context.Waiters
                    .ExcludeDeleted()
                    .FirstOrDefaultAsync(c => c.Id == request.WaiterId, cancellationToken);

                if (waiter == null)
                {
                    throw new NotFoundException(nameof(waiter), request.WaiterId);
                }

                var review = new Models.Review()
                {
                    Content = request.Content,
                    Rating = request.Rating,
                    WaiterReviews = new List<WaiterReviews>()
                    {
                        new WaiterReviews()
                        {
                            WaiterId = waiter.Id,
                        }
                    },

                    Created = now,
                    IsDeleted = false,
                    CreatedBy = currentUser,
                    Modified = now,
                    ModifiedBy = currentUser,
                };

                await _context.Reviews.AddAsync(review, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);

                await _mediator.Send(new UpdateWaiterAverageRatingCommand() { Waiter = waiter }, cancellationToken);

                return Unit.Value;
            }
        }

        public class Validator : AbstractValidator<CreateReviewCommand>
        {
            public Validator()
            {
                RuleFor(c => c.Content).NotEmpty().MaximumLength(1000);
                RuleFor(c => c.Rating).NotEmpty();
                RuleFor(c => c.WaiterId).NotEmpty();
            }
        }
    }
}