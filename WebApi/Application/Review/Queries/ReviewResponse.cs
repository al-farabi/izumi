﻿using AutoMapper;
using WebApi.Infrastructure.Automapper;

namespace WebApi.Application.Review.Queries
{
    public class ReviewResponse : IHaveCustomMapping
    {
        public string Content { get; set; }
        public byte Rating { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Models.Review, ReviewResponse>();
        }
    }
}