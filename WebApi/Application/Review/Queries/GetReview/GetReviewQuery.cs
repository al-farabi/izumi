﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;
using WebApi.Infrastructure.Exceptions;

namespace WebApi.Application.Review.Queries.GetReview
{
    public class GetReviewQuery : IRequest<ReviewResponse>
    {
        public Guid WaiterId { get; set; }
        public Guid ReviewId { get; set; }

        public class Handler : IRequestHandler<GetReviewQuery, ReviewResponse>
        {
            private readonly IzumiContext _context;
            private readonly IMapper _mapper;

            public Handler(IzumiContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ReviewResponse> Handle(GetReviewQuery request, CancellationToken cancellationToken)
            {
                var review = await _context.WaiterReviews
                    .AsNoTracking()
                    .Include(c => c.Review)
                    .FirstOrDefaultAsync(
                        c => c.WaiterId == request.WaiterId && c.ReviewId == request.ReviewId, cancellationToken);

                if (review == null)
                {
                    throw new NotFoundException(nameof(review), request.ReviewId);
                }

                return _mapper.Map<ReviewResponse>(review.Review);
            }
        }

        public class Validator : AbstractValidator<GetReviewQuery>
        {
            public Validator()
            {
                RuleFor(c => c.ReviewId).NotEmpty();
                RuleFor(c => c.WaiterId).NotEmpty();
            }
        }
    }
}