﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApi.Context;
using WebApi.Infrastructure.Extensions;

namespace WebApi.Application.Review.Queries.GetReviews
{
    public class GetReviewsQuery : IRequest<List<ReviewResponse>>
    {
        public Guid WaiterId { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public class Handler : IRequestHandler<GetReviewsQuery, List<ReviewResponse>>
        {
            private readonly IzumiContext _context;
            private readonly IMapper _mapper;

            public Handler(IzumiContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<ReviewResponse>> Handle(GetReviewsQuery request, CancellationToken cancellationToken)
            {
                var reviews = await _context.WaiterReviews
                    .AsNoTracking()
                    .Where(c => c.WaiterId == request.WaiterId)
                        .Include(c => c.Review)
                    .PageBy(c => c.Review.Created, request.Page, request.PageSize)
                        .Select(c => c.Review)
                    .ProjectTo<ReviewResponse>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                return reviews;
            }
        }
    }
}