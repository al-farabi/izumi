﻿using System;
using System.Collections.Generic;
using WebApi.Infrastructure.Common;

namespace WebApi.Models
{
    public class Company : BaseModel<Guid>
    {
        public string Name { get; set; }
        public CompanyType CompanyType { get; set; }

        public List<Waiter> Waiters { get; set; }
    }
}