﻿using System;
using System.Collections.Generic;
using WebApi.Infrastructure.Common;

namespace WebApi.Models
{
    public class Review : BaseModel<Guid>
    {
        public string Content { get; set; }
        public byte Rating { get; set; }

        public List<WaiterReviews> WaiterReviews { get; set; }
    }
}