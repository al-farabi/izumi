﻿using System;

namespace WebApi.Models
{
    public class WaiterReviews
    {
        public Waiter Waiter { get; set; }
        public Guid WaiterId { get; set; }

        public Review Review { get; set; }
        public Guid ReviewId { get; set; }
    }
}