﻿using System;
using System.Collections.Generic;
using WebApi.Infrastructure.Common;

namespace WebApi.Models
{
    public class Waiter : BaseModel<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double AverageRating { get; set; }

        public Company Company { get; set; }
        public Guid CompanyId { get; set; }

        public List<WaiterReviews> WaiterReviews { get; set; }
    }
}