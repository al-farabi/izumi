﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Application.Company.Commands;
using WebApi.Application.Company.Queries.GetCompanies;
using WebApi.Application.Company.Queries.GetCompany;
using WebApi.Application.Waiter.Queries.GetWaiter;
using WebApi.Application.Waiter.Queries.GetWaiters;
using WebApi.Controllers.Base;

namespace WebApi.Controllers
{
    [ApiVersion("1.0")]
    public class CompaniesController : BaseController
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int page = 1, int pageSize = 10)
        {
            return Ok(await Mediator.Send(new GetCompaniesQuery() { Page = page, PageSize = pageSize }));
        }

        [HttpGet("{companyId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(Guid companyId)
        {
            return Ok(await Mediator.Send(new GetCompanyQuery() { Id = companyId }));
        }

        [HttpGet("{companyId}/waiters")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetWaitersByCompanyId(Guid companyId, int page = 1, int pageSize = 10)
        {
            return Ok(await Mediator.Send(new GetWaitersQuery() { CompanyId = companyId, Page = page, PageSize = pageSize }));
        }

        [HttpGet("{companyId}/{waiterId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetWaiterByCompanyId(Guid companyId, Guid waiterId)
        {
            return Ok(await Mediator.Send(new GetWaiterQuery() { CompanyId = companyId, WaiterId = waiterId }));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Create([FromBody] CreateCompanyCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update([FromBody] UpdateCompanyCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("id")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await Mediator.Send(new DeleteCompanyCommand() { CompanyId = id });

            return NoContent();
        }
    }
}