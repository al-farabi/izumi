﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Application.Review.Commands;
using WebApi.Application.Review.Queries.GetReview;
using WebApi.Application.Review.Queries.GetReviews;
using WebApi.Application.Waiter.Commands;
using WebApi.Controllers.Base;

namespace WebApi.Controllers
{
    [ApiVersion("1.0")]
    public class WaitersController : BaseController
    {
        [HttpGet("{waiterId}/reviews")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetWaiterReviews(Guid waiterId, int page = 1, int pageSize = 10)
        {
            return Ok(await Mediator.Send(new GetReviewsQuery() { WaiterId = waiterId, Page = page, PageSize = pageSize }));
        }

        [HttpGet("{waiterId}/{reviewId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetWaiterReview(Guid waiterId, Guid reviewId)
        {
            return Ok(await Mediator.Send(new GetReviewQuery() { ReviewId = reviewId, WaiterId = waiterId }));
        }

        [HttpPost("review")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> AddReview([FromBody] CreateReviewCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Create([FromBody] CreateWaiterCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update([FromBody] UpdateWaiterCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update(Guid id)
        {
            await Mediator.Send(new DeleteWaiterCommand() { Id = id });

            return NoContent();
        }
    }
}