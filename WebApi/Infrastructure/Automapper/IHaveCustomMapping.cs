﻿namespace WebApi.Infrastructure.Automapper
{
    public interface IHaveCustomMapping
    {
        void CreateMappings(global::AutoMapper.Profile configuration);
    }
}
