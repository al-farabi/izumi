﻿using System;

namespace WebApi.Infrastructure.Exceptions
{
    public class BadRequestException : Exception
    {
        public string UserId { get; set; }

        public BadRequestException(string message, string userId)
            : base(message)
        {
            UserId = userId;
        }
    }
}