﻿namespace WebApi.Infrastructure.Exceptions
{  
    public class CustomException
    {
        public string Error { get; set; }
        public string Trace { get; set; }
    }
}
