﻿using System;
using System.Linq;
using System.Linq.Expressions;
using WebApi.Infrastructure.Common;

namespace WebApi.Infrastructure.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> WhereIf<T>(this IQueryable<T> query, bool condition,
            Expression<Func<T, bool>> predicate)
        {
            return condition
                ? query.Where(predicate)
                : query;
        }

        public static IQueryable<T> TakeIf<T, TKey>(this IQueryable<T> query, Expression<Func<T, TKey>> orderBy,
            bool condition, int limit, bool orderByDescending = true)
        {
            // It is necessary sort items before it
            query = orderByDescending ? query.OrderByDescending(orderBy) : query.OrderBy(orderBy);

            return condition
                ? query.Take(limit)
                : query;
        }

        public static IQueryable<T> PageBy<T, TKey>(this IQueryable<T> query, Expression<Func<T, TKey>> orderBy,
            int page, int pageSize, bool orderByDescending = true)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }

            page = ValidatePageNumber(page);
            pageSize = ValidatePageSize(pageSize);

            // It is necessary sort items before it
            query = orderByDescending ? query.OrderByDescending(orderBy) : query.OrderBy(orderBy);

            return query.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public static IQueryable<T> ExcludeDeleted<T>(this IQueryable<T> query)
            where T : IDeletable
        {
            return query.Where(c => !c.IsDeleted);
        }

        private static int ValidatePageNumber(int page)
        {
            const int defaultPageNumber = 1;

            // Check if the page number is greater then zero - otherwise use default page number
            return page <= 0 ? defaultPageNumber : page;
        }
        private static int ValidatePageSize(int pageSize)
        {
            const int minPageSize = 10;
            const int maxPageSize = 50;
            // Check if the page size is greater then 50 - otherwise use max page size
            if (pageSize > 50)
            {
                return maxPageSize;
            }

            // Check if the page size is less then 10 - otherwise use min page size
            if (pageSize < 10)
            {
                return minPageSize;
            }

            return pageSize;
        }
    }
}