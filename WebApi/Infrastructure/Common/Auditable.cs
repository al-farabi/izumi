﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Infrastructure.Common
{
    public abstract class Auditable
    {
        public DateTime Created { get; set; }
        [MaxLength(255)]
        public string CreatedBy { get; set; }
        public DateTime Modified { get; set; }
        [MaxLength(255)]
        public string ModifiedBy { get; set; }
    }
}