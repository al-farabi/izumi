﻿namespace WebApi.Infrastructure.Common
{
    public interface IDeletable
    {
        bool IsDeleted { get; set; }
    }
}