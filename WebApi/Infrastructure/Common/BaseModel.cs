﻿namespace WebApi.Infrastructure.Common
{
    public class BaseModel<T> : Auditable, IEntity<T>, IDeletable
    {
        public T Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}