﻿namespace WebApi.Infrastructure.Common
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}